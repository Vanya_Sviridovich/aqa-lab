Then took the other, as just as fair,
And having perhaps the better claim,
Because it was grassy and wanted wear;
Though as for that the passing there
Had worn them really about the same
And both that morning equally lay
In leaves no step had trodden black.
"Whether 'tis nobler in the mind to suffer
The slings and arrows of outrageous fortune,"
"Or to take Arms against a Sea of troubles,
And by opposing end them: to die, to sleep;"

